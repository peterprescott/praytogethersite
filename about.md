---
layout: layout.liquid
---



PrayTogether is a a SIMPLER invitation to Spiritual Discovery for everyone everywhere.

If you'd like to connect, then [send me an email](mailto:hello@praytogether.uk) or join the [WhatsApp group](https://chat.whatsapp.com/Gb7Uld7Q4TVDLu7PdBa2Fp).

<hr>

This website was hastily assembled by [Peter Prescott](https://peter.prescott.ws)   
on the afternoon of April 10th 2019, after Nic Harding had suggested the day before  
that I think in terms of 1. tools, 2. stories, and 3. opportunities,  
to help i. people's personal prayer, ii. people pray with friends, iii. leaders prayer together.  

So far this is intended to address (1), mainly with regards to (i).
Let me know if it does actually help!

<hr>

It is statically generated using [11ty](https://www.11ty.io/) and hosted for free by [Netlify](https://www.netlify.com/).   
Feel free to [use git](https://www.youtube.com/watch?v=BCQHnlnPusY&list=PLRqwX-V7Uu6ZF9C0YMKuns9sLDzK6zoiV&index=2&t=0s) to clone it from [its GitLab repository](https://gitlab.com/peterprescott/praytogethersite) and redeploy it for the glory of God.  
As you can probably see, I'm not much of a front-end web-designer.  
Offers of assistance are welcome, though i won't give you any money ;)

