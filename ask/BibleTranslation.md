---
date: "2018-11-12T12:34:06Z"
title: "Bible Translation"
---

“Everyone who calls on the name of the Lord will be saved.” But how then will they call on him in whom they have not believed? And how are they to believe in him of whom they have never heard? And how are they to hear without someone preaching? Romans 10:13-4

And by the same logic, how will they understand that preaching unless it brings the Word of God in a language that is understood?

So let's pray for the work of [Wycliffe Bible Translators](https://www.wycliffe.org/prayer), and for [the 1.5 billion people who do not have the full Bible in their own language](https://www.wycliffe.org/about/why).
