---
date: "2018-11-08T23:00:12Z"
title: "Flooding in France"
---

_Again he measured a thousand, and it was a river that I could not pass through, for the water had risen. It was deep enough to swim in, a river that could not be passed through. “Do you see this, son of man?” he asked._ Ezk. 47:5-6

Prompted by [a prayer request](https://mailchi.mp/9344d7c22ee1/flooding-update?e=8d55746015) from missionaries in Carcassonne, France, we prayed for those that have been hit by severe floods. And then we continued praying for the living water of the Holy Spirit to break the dam of secularism and flood the nation.

[PrayerCast video](https://www.prayercast.com/france.html )
[OperationWorld](http://www.operationworld.org/country/fran/owtext.html)
