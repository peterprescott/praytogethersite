---
date: "2018-11-26T16:51:02Z"
title: "Leadership Training in India"
---

(Video from [PrayerCast](https://www.prayercast.com/india.html))

[Operation World](http://www.operationworld.org/country/indi/owtext.html): Training Christian workers is an important need that is immediately urgent and essential in the long term. The life and health of the Church depend on the proper development of pastors, teachers, evangelists and missionaries. In churches, poor discipling and lack of teaching and modelling of biblical life and leadership are problems. India’s strong philosophical tradition and religious, cultural and ethnic diversity make adequate training crucial, but most workers are sent out with very little specific preparation for their ministry context.
