---
date: "2018-11-19T11:25:21Z"
title: "Israel & Gaza"
---

JG said on 13/11:
_We just returned from Israel. Please pray for our family and friends who have to be in bomb shelters today as the missiles continue to fall [note: [a ceasefire has now been agreed](https://www.theguardian.com/world/2018/nov/13/relentless-reprisal-attacks-as-israel-and-hamas-ignore-ceasefire-pleas-gaza)]. For the families of those killed, and for the Arabs in Gaza under the thumb of Hamas._

Operation World: [Israel](http://www.operationworld.org/country/isra/owtext.html), [Palestine](http://www.operationworld.org/country/pale/owtext.html)
PrayerCast: [Israel](https://www.prayercast.com/israel.html), [Palestine](https://www.prayercast.com/gaza-strip-and-west-bank.html)
