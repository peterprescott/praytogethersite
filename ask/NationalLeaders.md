---
date: "2018-11-08T22:58:09Z"
title: "National Leaders"
---

_Pray for kings and all those in authority, so that we may lead tranquil and quiet lives in all godliness and dignity._ 1 Tim. 2:2

We prayed for Donald Trump and the USA. Theresa May here in the UK. Vladimir Putin in Russia. Angela Merkel in Germany.  There’s a [full-list of heads of state here](https://en.wikipedia.org/wiki/List_of_current_heads_of_state_and_government 
).
