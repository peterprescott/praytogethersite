---
date: "2018-11-08T22:24:12Z"
title: "Operation World"
---

_Or what king, going out to war, will not sit down first and deliberate whether he is able to fight the battle..._	Luke 14:31

No-one has made a more rigorous attempt to survey the task of Asking For The Nations than Patrick Johnstone, pioneering author of Operation World, the definitive guide to missionary prayer. As we begin establishing ‘a house of prayer for the nations’ in Liverpool (indeed in a bookshop, where we have at least four editions of Operation World!), I want to honour Patrick Johnstone as a hero of the faith whose example I want to emulate.
