---
date: "2018-12-03T21:23:04Z"
title: "Religious Freedom in Pakistan"
---

[Asia Bibi](https://www.theosthinktank.co.uk/comment/2018/11/12/a-battle-for-the-soul-of-pakistan-the-asia-bibi-case-explained), a Pakistani Christian woman accused of blasphemy, has recently been in the news after the Supreme Court acquitted her.

So let's pray for her, and for religious freedom in Pakistan, and for the various ministries that support persecuted Christians ([the Barnabas Fund](https://barnabasfund.org/en/about/who-we-are), [Open Doors](https://www.opendoors.org/)) and advocate for religious freedom ([Christian Solidarity Worldwide](https://www.csw.org.uk/home.htm) -- their [briefing on Pakistan](http://docs-eu.livesiteadmin.com/dc3e323f-351c-4172-800e-4e02848abf80/2018-09-general-briefings-pakistan.pdf) is very insightful).

And as ever, [Operation World](http://www.operationworld.org/country/paki/owtext.html) and [PrayerCast](https://www.prayercast.com/pakistan.html) are vital resources to pray.
