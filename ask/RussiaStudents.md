---
date: "2018-11-08T23:02:41Z"
title: "Student Ministry in Russia"
---

_See, I am doing a new thing! Now it springs up; do you not perceive it? I am making a way in the wilderness and streams in the wasteland._	Isaiah 43:19

Prompted by a prayer request from [IFES](https://ifesworld.org/en) Russia to join in with their World Student Day, we prayed for student ministry in Russia. See [their specific prayer requests here](https://ifeswsd.org/pray?region=eurasia).

And then we continued to pray for the nation, for there to be a hunger for truth and the freedom to ask questions and discover answers.

[PrayerCast video](https://www.prayercast.com/russia.html) 
[OperationWorld](http://www.operationworld.org/country/russ/owtext.html)
