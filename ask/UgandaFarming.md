---
date: "2018-11-08T23:03:38Z"
title: "Farming in Uganda"
---

_A farmer went out to sow his seed…_	Luke 8:5

One of our team is connected to a farming project in Uganda. So we prayed for those involved with that and for the nation, and in particular for a healthy biblical understanding of the value of work and a proper theology of prosperity. 

[PrayerCast video](https://www.prayercast.com/uganda.html )
[OperationWorld](http://www.operationworld.org/country/ugan/owtext.html )
