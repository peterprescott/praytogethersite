---
date: "2018-11-12T15:38:47Z"
title: "Fatherlessness"
---

NF in [Norris Green](https://en.wikipedia.org/wiki/Norris_Green) says: "Would be great to pray into the issue of **fatherlessness** at some point in your meetings. So much pain and brokenness amongst so many here because of difficult family dynamics and abandonment."

Almost half of all British children born today will not be living with both parents by the time they are 15 ([Guardian, Feb 2017](https://www.theguardian.com/lifeandstyle/2017/feb/12/fatherless-society-children-in-poverty-iain-duncan-smith--social-justice-thinktank)). 

May this nation know God as a Father to the Fatherless [Psalm 68:5](https://biblehub.com/psalms/68-5.htm).
