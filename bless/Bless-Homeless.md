---
date: "2018-11-12T12:13:42Z"
title: "Homelessness"
---

As it gets colder, we're praying for the homeless.

In particular, blessing the good work done by Labre House Night Hub and the [Whitechapel Centre](https://www.whitechapelcentre.co.uk/).

And remembering too the [Homeless Outreach](http://www.liverpoolhomelessoutreach.co.uk/) that happens out of 18 Slater Street on Sunday nights, and the one run by Deeper Life Church on Thursdays.
