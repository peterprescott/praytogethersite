---
date: "2018-11-08T22:23:16Z"
title: "The (Local) Nations"
---

_Now there were dwelling in the city… men from every nation under heaven._ Acts 2:5

According to [Wikipedia](https://en.wikipedia.org/wiki/Demography_of_Liverpool), Liverpool’s demography includes Chinese (the oldest Chinese community in Europe), Afro-Caribbeans, Ghanaians, Somalis, Zimbabweans, Yemenis, Indians (Tamils, Malayalis, Punjabis…), Pakistanis, Bangladeshis, Malaysians, Vietnamese, Greeks, Italians, Brazilians, Colombians, Bolivians, Peruvians, Argentines, Cubans. Are all these nations represented in the churches of our city? Where are there opportunities right here on our doorsteps to begin [engaging with unreached people groups?](https://joshuaproject.net/countries/UK).
