---
date: "2018-11-08T22:22:19Z"
title: "The Slave Trade"
---

_We acknowledge our wickedness, O LORD, and the iniquity of our fathers, for we have sinned against you._Jer. 14:20

Our meeting place for prayer in the centre of Liverpool is changing its name to Tree of Life, having been previously known as Gladstone’s, after one of the founding trustees, [Sir John Gladstone](https://en.wikipedia.org/wiki/Sir_John_Gladstone%2C_1st_Baronet). It turns out he was a slave-trader and –owner – in fact, he received the largest of all the compensation payments that were made as part of the deal to secure the Slavery Abolition Act. We don’t want to whitewash our history, but to understand what historical implications still need to be addressed, trusting that what the enemy meant for evil, God is able to turn to good (Gen. 50:20).
