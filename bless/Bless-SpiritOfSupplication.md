---
date: "2018-11-08T22:21:29Z"
title: "Revelation of the Cross"
---

_I will pour out … a spirit of grace and supplication, and they will look on Me, the One they have pierced. They will mourn for Him as one mourns an only child, and weep bitterly for Him as one grieves a firstborn son._ Zech. 12:10

We had a powerful time praying that God would give everyone (believer or not) in Liverpool a deep revelation of the Cross.
