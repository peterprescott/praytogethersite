---
date: "2018-11-12T23:26:19Z"
title: "Student Mission"
---

<center>![](12-5-1.jpeg)</center>

Prompted by news of [Edge Hill Christian Union](https://www.edgehillsu.org.uk/groups/christian-union-6eed)'s Mission Week, I thought we should pray for the students of Liverpool's various universities ([University of](https://www.liverpool.ac.uk/), [Edge Hill](https://www.edgehill.ac.uk/), [John Moores](https://www.ljmu.ac.uk/), [Hope](http://www.hope.ac.uk/)), and the ministries (~~[Agape](http://www.agape.org.uk/)~~, [Fusion](https://www.fusionmovement.org/), ~~[Navigators](https://navigators.co.uk/students/)~~, [UCCF](https://www.uccf.org.uk/)) and churches ([St James in the City](https://www.stjamesinthecity.org.uk/Group/Group.aspx?ID=291312), [Frontline](http://www.frontline.org.uk/)'s [Student Chur.ch](http://www.studentchur.ch/).. ack, I'm too new to the city to know the major student churches!) focussed on reaching them.
