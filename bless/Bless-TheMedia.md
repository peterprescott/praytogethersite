---
date: "2018-11-26T16:26:45Z"
title: "The Media"
---

In the words of Christians in Media: _Love it or hate it, media plays a vital role in our lives. At its best, media gives a voice to the voiceless, holds the powerful to account, and highlights unjust practices while entertaining us and keeping us informed. As Christians, we affirm and support the media’s vital role in our society. We promote the highest standards in the media, and give our backing to the vast majority of people working in media as they strive for integrity and truth. From national newspapers to local radio, from websites to specialist publications, from TV networks to blogs, all have a valuable role to play. A thriving global and national media matters to all of us. We want to know about our world, its celebrations, its problems and its joys, and we need a thriving media to help us engage with it._

In Liverpool we want to pray for our local media, such as newspaper [The Echo](https://www.liverpoolecho.co.uk/), and Christian radio station [Flame Radio](http://flameradio.org/); as well as those national and global media that inform and influence us.
