---
date: "2018-12-03T21:15:56Z"
title: "Restoring Vulnerable People's Lives"
---

Agnes has shared some prayer requests this week about one of the people connected to the ministry she works with, [City Hearts](http://city-hearts.co.uk/). So we're praying for them, and more broadly for all the ministries and social services that work Restoring Vulnerable People's Lives in our City Region.
