---
path: "/pages/2018/11/worship/"
date: "2018-11-08T22:20:47Z"
title: "Worship"
categories: []
tags: ['bless']
excerpt: "_“The hour is coming, and is now here, when the true worshipers will worship the Father in spirit a..."
single: true
---

_“The hour is coming, and is now here, when the true worshipers will worship the Father in spirit and truth, for the Father is seeking such people to worship him.”_ John 4:23.

Real intercessory prayer is hard work—the Bible compares it (Isaiah 21:3-6) to being like a woman in labour. If we are to persevere in prayer until real breakthrough comes, then prayer has to be not just a duty but a delight. Which means it needs to be infused and empowered with worship. 
So it’s been a real encouragement that from the very first morning of beginning to establish prayer in the centre of Liverpool, we’ve been joined by Dionne, who has since begun connecting and equipping worship leaders on the Wirral and beyond. See [a video of her sharing her story here](https://drive.google.com/file/d/1Zn2wkxr8TWtspzGzUtK5CM5bSOLV5IxM/view?usp=drive_web ).
