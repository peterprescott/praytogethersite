---
date: "2018-11-08T22:53:46Z"
title: "Abortion"
---

_You knit me together in my mother’s womb…_ Psalm 139:13

Prompted by a call to pray for [Fiona Bruce MP](https://www.fionabruce.org.uk/) as she responds to [Diana Johnson MP’s proposal to radically reduce regulation of abortion](https://www.spuc.org.uk/news/news-stories/2018/october/mps-use-every-means-to-further-extreme-abortion-agenda), meaning that no reason at all would be required for killing an unborn baby less than 24 weeks of age. 

Having just had the 20-week scan for the boy (we can now confirm!) with whom my wife is currently pregnant, this felt unusually personal.
