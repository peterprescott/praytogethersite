---
date: "2018-11-19T11:38:02Z"
title: "The Withdrawal Deal (19 Nov)"
---

The Prime Minister has presented Parliament with a [Draft Withdrawal Agreement](https://www.gov.uk/government/publications/draft-withdrawal-agreement-19-march-2018).

What happens next? It has been agreed by Mrs May's Cabinet, and should be endorsed by the EU Summit on [25 Nov](https://www.theguardian.com/world/2018/nov/15/brexit-summit-finalise-deal-donald-tusk) -- but whether Parliament will approve it is uncertain.

_Pray for kings and all those in authority, so that we may lead tranquil and quiet lives in all godliness and dignity._ 1 Tim. 2:2.

_The hearts of kings are like water in the hand of the Lord; He turns it wherever He will._ [Proverbs 21:1](https://biblehub.com/proverbs/21-1.htm)
