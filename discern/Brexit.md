---
date: "2018-11-08T22:57:08Z"
title: "Brexit"
---

_Speak to Zerubbabel, governor of Judah, saying, I am about to shake the heavens and the earth…_	Haggai 2:21

29 March 2019 is the date that Britain is to leave the European Union. Whatever exact deal is or isn’t in place before that, there is clearly going to be some sever economic and political shaking. It is clear that Britain lacks a healthy understanding of its [historic](https://en.wikipedia.org/wiki/Ecclesiastical_History_of_the_English_People) and [constitutional](https://en.wikipedia.org/wiki/Coronation_of_the_British_monarch#Recognition_and_oath) Christian national identity, and so lacks defence against implicitly atheistic (thus groundless) humanism on the one side and bigoted racism on the other. 

We need God to raise up [wise](http://www.jubilee-centre.org/brexit-fractured-europe-paul-mills-michael-schluter/) and [prophetic](https://duckduckgo.com/?q=brexit+prophecy&t=ironbrowser&ia=web) Christian voices now perhaps more than ever, both to speak into the specific political situation of Brexit, and more importantly to declare the simple gospel. Especially as the false foundations of economic and political security are shaken. 

See also:
- [The Withdrawal Deal (19 Nov)](../Brexit-WithdrawalDeal)
