---
date: "2018-11-26T16:08:43Z"
title: "#JohnAllenChau"
---

[Social media](https://twitter.com/hashtag/JohnAllenChau?src=hash) has been all achatter after the news of the death of 26-year-old American missionary John Allen Chau, after [his short-lived attempt to reach an isolated island tribe with the gospel ended in his death](https://www.theguardian.com/world/2018/nov/22/john-allen-chau-man-killed-by-tribe-north-sentinel-island-declare-jesus).

[Dave Miller, a Southern Baptist pastor](https://sbcvoices.com/john-allen-chau-martyrdom-and-missionary-efforts/
) has some sensible reflections, affirming that "the ultimate commitment of all Christians is to carry the gospel to all peoples", while questioning "the wisdom and strategic efficacy of Chau’s approach". But let's remember that [even the apostle Paul (Acts 28:1-10)](https://www.biblegateway.com/passage/?search=Acts+28%3A1-10) found taking the gospel to tribal islanders to be dangerous and unpredictable.

One thing clearly demonstrated by the story is how contemporary communication technology puts even mission to the most inaccessible peoples in full view of the global public eye. And unsurprisingly, people of different perspectives come to different conclusions. Compare and contrast [The Independent's disdainful judgment](https://www.independent.co.uk/voices/john-allen-chau-death-tribe-missionary-christian-north-sentinel-island-uncontacted-a8648691.html
) with [Christianity Today's slightly hagiographical summary](https://www.christianitytoday.com/news/2018/november/missionary-killed-north-sentinel-isolated-island-tribe-chau.html). So let's pray for wisdom [to communicate well about cross-cultural mission](https://www.ywamkb.net/kb/3D_Communications), and to do it in such a way that whatever happens [the message of divine forgiveness will be amplified](https://www.indiatoday.in/india/story/american-man-john-allen-chau-family-forgive-sentinelese-andaman-murder-1394119-2018-11-22). 

Whether or not Chau deserves to be called a _martyr_, in the same category as [Jim Elliot](https://en.wikipedia.org/wiki/Jim_Elliot) or [Graham Staines](https://en.wikipedia.org/wiki/Graham_Staines), it remains true that **he is no fool who gives what he cannot keep to gain that which he cannot lose**. Quoting Dave Miller again: _What he did was foolhardy but look how often God called his people to foolhardy acts in Scripture. Most often, he protects his people but sometimes even the obedient met messy ends. There are not guarantees that serving God is safe._

Let's pray that God would continue to give Christians boldness to reach the unreached and wisdom to do it effectively; and that [every tribe](https://omf.org/blog/2018/11/22/love-your-neighboring-tribe/) would hear the message of Jesus communicated in a culturally appropriate and intelligible way.
