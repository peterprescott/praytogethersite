---
date: "2018-11-08T22:51:48Z"
title: "Marriage"
---

_Jesus answered, “Have you not read that he who created them from the beginning made them male and female, and said, ‘Therefore a man shall leave his father and his mother and hold fast to his wife, and the two shall become one flesh’? So they are no longer two but one flesh. What therefore God has joined together, let not man separate.”_ Matthew 19:4-6

Prompted by Theresa May’s announcement that the Government plans to introduce civil partnerships for heterosexual partners, we prayed into the vexed question of marriage today. The Government’s plans following the [UK Supreme Court’s ruling](https://www.theguardian.com/lifeandstyle/2018/jun/27/uk-ban-on-heterosexual-civil-partnerships-ruled-discriminatory) that the current situation (whereby gay couples can have either a civil partnership or a marriage, while heterosexual couples looking to legally formalize their relationship only have the option of marriage) is discriminatory. 

Praying for Christians to understand and submit to Jesus’ definition of marriage (Matt.19:4-6) as being the voluntary, permanent and exclusive union of one man and one woman; while knowing how to wisely, humbly and graciously engage with a world that defines marriage differently. Praying mercy for those in messy relationships that don’t have the divine blessing of God-ordained marriage, and safety for the children of such relationships. Praying strength for those who are married, for non-Christians and Christians.

A few days after we’d been praying into this topic, the [UK Supreme Court gave their verdict](https://www.supremecourt.uk/cases/docs/uksc-2017-0020-press-summary.pdf) on the ‘Gay Cake’ case involving Asher’s Bakery, concluding that they were not liable for refusing to bake a cake supporting gay marriage, and that they had not discriminated against the customer on the grounds of sexual orientation (which would have been illegal), but had merely exercised their right to refuse to express a political opinion contrary to their religious beliefs. An answer to prayer?
