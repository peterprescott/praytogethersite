---
date: "2018-11-08T22:50:31Z"
title: "The Prayer Movement"
---

_“My house will be a house of prayer for all the nations”._ Isaiah 56:7 (~Mk.11:7//Lk.19:46//Mt.21:13)

All over the world the Holy Spirit is highlighting the phrase ‘house of prayer’ like never before. [IHOPKC](https://ihopkc.org), [Ffald-y-Brenin](http://www.ffald-y-brenin.org/), [24/7 Prayer](https://www.24-7prayer.com/), etc. Some of our team went to connect with other Houses of Prayer from round the UK at the annual Gathering hosted by [Beacon House of Prayer](http://www.beaconhop.org/) in Stoke.
