---
date: "2018-11-12T12:00:19Z"
title: "Refugee Crisis"
---

The Syrian Refugee Crisis is the one that has forced itself to the world's attention, but this is a global issue: One in a hundred of the whole global population is a refugee.

The President of [the International Rescue Committee](https://www.rescue.org/) suggests that priorities are:
- allowing refugees to work, so they can support themselves,
- providing education for refugee children,
- supporting poorer countries who are most impacted by refugees.

- One thing God is doing in the midst of this crisis is opening up opportunities for Muslims to be reached with the gospel.
- There is a need for Christian believers to serve, whether [on the front line](https://www.ywamlesvos.com/ministries), or [closer to home](https://www.gov.uk/help-refugees).
