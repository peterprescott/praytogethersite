---
date: "2018-11-08T22:55:26Z"
title: "Technology"
---

_Paul stayed and worked with them, because they were tentmakers by craft (techne, from which we get the word technology), just as he was._ Acts 18:3

Prompted by the ‘hackathon’ organized by [Kingdom Code](https://kingdomcode.uk/) that I was able to attend in London, part of [a global movement of such events in 30+ cities bringing together Christians in technology](https://indigitous.org/2018/10/24/indigitous-hacks-around-the-world/).

Praying for people like [Andy Geers and his team](https://discipleship.tech/) as they develop technology to help people grow in their relationship with Jesus. Praying for all of us who use different technologies--if Facebook’s daily users were a nation, it would the biggest in the world, with [almost 1.5 billion](https://www.statista.com/statistics/346167/facebook-global-dau/).
