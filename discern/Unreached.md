---
date: "2018-11-09T20:29:50Z"
title: "Movements among Unreached"
---

_"The gospel of the kingdom will be preached as a testimony to all nations, and then the end will come."_
Matthew 24:14

[24:14](https://www.2414now.net/) is aiming to see Kingdom Movements begun among every unreached people group by 31 December 2025\. I was struck by the parallel with [Together For The Harvest](http://tfh.org.uk)'s goal of reaching 'every man, woman and child in the Liverpool City Region' with the gospel by (1 Jan?) 2026.
