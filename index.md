---
layout: layout.liquid
---

 # What's the vision?
 ## Every man, woman and child</a>  
 ## encountering Jesus.  
<hr>

 # The Challenge?
## Encountering Jesus can seem complicated and hard.
<hr>

# The Solution?
## Make it [SIMPLER](simpler).

