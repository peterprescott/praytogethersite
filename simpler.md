---
layout: layout.liquid
---

# Make prayer SIMPLER. [>>](scripture)
## "*When you pray, do not use vain repetition...*
## *Your Father already knows what you need.*"
#### [Jesus, Sermon on the Mount](https://www.biblegateway.com/passage/?search=matthew+6%3A7-15&version=ESV).

<hr>

# Prayer is not about tools.
## "*Only **#OneThing** is necessary.*" [Jesus](https://www.biblegateway.com/passage/?search=luke+10%3A42&version=ESV).


<hr>

# But "*we don't know how to pray as we ought*"! [Paul](https://www.biblegateway.com/passage/?search=romans+8%3A26&version=ESV).
### Well okay, here is a framework to help  
### make powerful, enjoyable prayer SIMPLER:
#### [**S**cripture](scripture). 
#### [**I**nformation](information).
#### [**M**usic](music).
####  [**P**attern](pattern). 
#### [**L**ists](lists).
####  [**E**ach Other](eachother).
####  [**R**hythms](rhythms).
