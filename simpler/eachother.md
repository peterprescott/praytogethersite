# [ << ](../lists) **E**ach other [ >> ](../rhythms)
<hr>

Because Matt. 18:19.
Praying personally is wonderful, but when we gather together in unity to pray together we enter a different dimension.

# Face-to-Face.
3 Basic Categories:
1. Your House (People you live with) -- what are the daily rhythms of life and prayer in your house?
2. Your Church -- what are the weekly rhythms of gathering in your church?
3. Others -- what other rhythms of prayer, mission and discipleship do you have?

# Long-Distance.
Anyone can (and *perhaps* everyone should) send a prayer-letter ([I've been doing this since 2010](https://prescott.ws/prayerletters/2010/09/02/the-beginning-peters-prayer-letter/)) to as many people as possible.  
(Do you need help getting started with the necessary technology so your impact can be [multiplied? We can help.](https://pi.prescott.ws))

Whose prayer-letters do you receive?

What other Christian writers (or podcasters, bloggers, or video-loggers) do you read who help inspire you to pray?

One of the unique dynamics of the age in which we live is its unprecedented communication technology: the Internet and Social Media. How do we take advantage of the opportunities and navigate the challenges of these?


