# [ << ](../scripture) **I**nformation [ >> ](../music)
<hr>



# Less is More. 
### Data is not Information.
<hr>

# Start SLOW. 
### **S**tart **L**ocal, **O**btain **W**isdom.

#### Pray for your Postcode.
- Who are the people you know in your postcode? How can you bless them?
- What are the churches in your postcode? And who is responsible for them? How can you bless them?
- What are the schools in your postcode? And who is responsible for them? How can you bless them?
- What are the businesses in your postcode? And who is responsible for them? How can you bless them?
- What are the social services in your postcode? And who is responsible for them? How can you bless them?
- Et cetera, etc., &c.
<hr>

# ASK: **A**cquiring **S**pecific **K**nowledge 
# requires Asking Specific Questions.
As you begin to pray for specific things in specific places, you will find that you need specific information to give you the wisdom you need to bless effectively. The best way to obtain specific information is to ask specific questions. Ask people you know, ask people you don't yet know!, ask [the internet](https://duckduckgo.com/?q=how+should+i+pray&t=h_&ia=web), ask for [books on the relevant topic](https://wordandspirit.co/order/).
<hr>

# Map your Circuit
*And Samuel went on a circuit year by year to Bethel, Gilgal, and Mizpah.* [1 Sam.7:16](https://biblehub.com/1_samuel/7-16.htm)  

What places has God called you to?

<input onclick="document.getElementById('destination').value=''" placeholder="enter UK postcode" type=text id="destination"> <a type=button class="btn-success" onclick="postcode()">*FLY!*</a>

- your postcode
- neighbouring postcodes
- your city centre
- neighbouring cities
- other [major cities](https://en.wikipedia.org/wiki/List_of_largest_cities), eg. <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(beijing)">Beijing</a>, <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(tokyo)">Tokyo</a>, <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(delhi)">Delhi</a>, <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(manila)">Manila</a>, <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(london)">London</a>, <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(newyork)">New York</a>
- your region
- neighbouring regions
- your nation
- neighbouring nations
- [unreached](https://joshuaproject.net/pray/unreachedoftheday/today) nations
- <a style="color: #5cb85c; text-decoration: underline;" onclick="fly(jerusalem,8)">Jerusalem</a> and the surrounding nations

<div id="map" class="map"></div>
<script type="text/javascript">
var liverpool = ol.proj.fromLonLat([-2.9341, 53.4179])
var view = new ol.View({
center: liverpool,
zoom: 13
});
var map = new ol.Map({
target: 'map',
layers: [
new ol.layer.Tile({
preload: 4,
source: new ol.source.OSM()
})
],
// Improve user experience by loading tiles while animating. Will make
// animations stutter on mobile or slow devices.
loadTilesWhileAnimating: true,
view: view
});
</script>
