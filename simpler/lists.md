# [ << ](../pattern) **L**ists [ >> ](../eachother)
<hr>


Keep lists of what you are praying for,  
and how God is responding (with breakthrough and with revelation).
*'What you see, write'.* Rev. 1:11.

#### Here are some ideas to start you off.

# 0. [Ask God for His list](0)
# 1. [Yourself](1)
# 2. ['Bless the City...'](2)
# 3. ['Ask for the Nations...'](3)
# 4. ['Discern the Times...'](4)

