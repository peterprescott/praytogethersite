# [ << ](../information) **M**usic [ >> ](../pattern)
<hr>


### *When the musician played, the hand of the Lord came upon Elisha.* 
#### [(2 Kings 3:15.)](https://biblehub.com/2_kings/3-15.htm)
<hr>

With just a few [simple chords](https://www.worshipchords.net/) anyone can help others enter the place of worship.  
Do this consistently and God will give you new songs to express what no-one else has yet put into words. ([Here are ours](https://music.prescott.ws)).  
But musical preference is a very culturally sensitive issue, so some people have strong opinions on this matter.  
This principle applies: *As for the one who is weak in faith, welcome him, but not to quarrel over opinions.* [(Rom. 14:10)](https://www.biblegateway.com/passage/?search=romans+14%3A1&version=ESV)
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLCX5FfkPoLhxREft365MD_UiP5bZRj57e" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/FWXr4NuZOCM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/oxmA4NtqR7s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/LLCEEIsEDjg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/go1-BoDD7CI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLF9A2939BF356D5E6" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ID5rrL1U-xI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>

<iframe width="560" height="315" src="https://www.youtube.com/embed/enIczFUEzAk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>


