# [ << ](../music) **P**attern [ >> ](../lists)
<hr>


Moses had to do everything according to the pattern he saw. [(Heb. 8:5)](https://biblehub.com/hebrews/8-5.htm).

But now through Christ's finished work on the cross, we have direct unmediated access to God. [(Heb. 8:6)](https://biblehub.com/hebrews/8-6.htm).

No-one else's religious framework should bar you from entering the fulness of joy that is promised for you in God's presence.  
(And whenever two or more people come together explicitly in the name of Jesus, then you are in God's presence [Matt.18:20]()).

But each of us is different--what feels like lifeless legalism to one person might be a helpful habit for another;  
what feels like the freedom of the Holy Spirit to one person might seem like chaos to another.

Some people find it vital to pray early in the morning,  
others can only manage later in the day.  
Some people find it helpful to use some liturgical structure,  
others just need the freedom to pray in tongues and they can then pray for hours!

I was asking the Lord for a pattern for corporate prayer that would help sustain LIFE,  
and He said 'Liturgy, Intercession, Free Prayer, and Each Other'.  
I said 'But I'm BAD at intercession',  
and He said, 'Bless the City, Ask for the Nations, Discern the Times'.  
Feel free to [adapt this pattern](LIFEliturgy.doc) however you like.

<hr>

Other patterns of prayer that people have said they find helpful: [ACTS](http://prayercentral.net/engage-me/ways-to-pray/pray-with-acts/), [1 Hour Prayer Wheel](http://strongrockchristianschool.com/wp-content/uploads/2016/08/1_HourPrayerWheel.01.pdf), [The CofE Daily Office](https://www.biblegateway.com/reading-plans/bcp-daily-office/today?version=esv), [Harp & Bowl](https://www.nfhop.org/PDF/Overview%20of%20Harp%20and%20Bowl.pdf), [Shane Claiborne's Liturgy](http://commonprayer.net/)--and [please suggest others](mailto:hello@praytogether.uk) you've found helpful!
