# [ << ](../eachother) **R**hythms. 
<hr>

Q: How do we sustain Constant Communion with the Lord?  
A: By Regular Rhythms of prayer.

## Daily Rhythms. 
Waking. Morning Rituals. 'Quiet Time'?. Work. Mealtimes (/Fasting). Evenings. Bedtime. 

## Weekly Rhythms. 
Sabbath. 'Church'. 'Home Group'. Prayer Meetings. Discovery Groups.

## Seasonal Rhythms. 
Spring,Summer,Harvest,Winter.  
The school year. The church calendar. Work and holidays.

## Annual Rhythms. 
God called Israel to celebrate '3 Festivals'.
The church traditionally celebrates Christmas, Easter & Pentecost.
There are also various Christian festivals through the year.

## Life Stages. 
[Unborn](https://biblehub.com/luke/1-41.htm).  
[Newborn](https://biblehub.com/psalms/22-9.htm).  
[Child](https://biblehub.com/matthew/19-14.htm).  
[Teenager](https://www.biblegateway.com/passage/?search=2+Chronicles+34%3A1-3&version=NIV).  
[Student](https://www.biblegateway.com/passage/?search=daniel+1%3A5-20&version=NIV).  
[Single](https://www.biblegateway.com/passage/?search=1+Corinthians+7%3A7-9&version=NIV).  
Unemployed.  
'Between things'.  
Married no kids.  
Young Family.  
Parents with Teenagers.  
Empty Nest.  
[Retired](https://www.biblegateway.com/passage/?search=luke+2%3A36-38&version=ESV).  
[Old Age](https://www.biblegateway.com/passage/?search=2+timothy+4%3A1-8&version=ESV).  
[Near Death](https://www.biblegateway.com/passage/?search=Luke+23%3A39-43&version=ESV).
[Death](https://biblehub.com/psalms/116-15.htm).  
[Resurrection](https://biblehub.com/1_corinthians/13-12.htm).  
