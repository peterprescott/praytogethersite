# [ << ](../) **S**cripture [ >> ](../information)
<hr>


Prayer is a two-way conversation. Don't just talk all the time -- learn to *listen*! Scripture helps us do both.
<hr>

# Scripture helps us speak to God.
#### *Take words with you, and return to the Lord...* [Hosea 14:2]()

### [The Sinner's Prayer](https://www.biblegateway.com/passage/?search=Luke+18%3A13-14&version=ESV):
#### [David](https://www.biblegateway.com/passage/?search=psalm+51&version=ESV), [Isaiah](https://www.biblegateway.com/passage/?search=isaiah+6%3A5-8&version=ESV), [Simon Peter](https://www.biblegateway.com/passage/?search=luke+5%3A8-10&version=ESV), [Paul](https://www.biblegateway.com/passage/?search=1+timothy+1%3A12-17&version=ESV), [John](https://www.biblegateway.com/passage/?search=revelation+5%3A4-5&version=ESV).
### [The Lord's Prayer.](https://www.biblegateway.com/passage/?search=matthew+6%3A7-15&version=ESV)
### [Apostolic Prayers.](http://www.mikebickle.org.edgesuite.net/MikeBickleVOD/2008/Key_Apostolic_Prayers_and_Intercessory_Promises.pdf)
### [Covenant](https://www.biblegateway.com/passage/?search=leviticus+26%3A40-45&version=ESV) Prayers: 
#### [Moses](https://www.biblegateway.com/passage/?search=exodus+32%3A31-34%3B+33%3A12-23%3B+34%3A5-10&version=ESV), [Hannah](https://www.biblegateway.com/passage/?search=1+Samuel+2%3A1-10&version=ESV), [David](https://www.biblegateway.com/passage/?search=1+Chronicles+29%3A10-18&version=ESV), [Solomon](https://www.biblegateway.com/passage/?search=2+Chronicles+6%3A14-42&version=ESV), [Isaiah](https://www.biblegateway.com/passage/?search=isaiah+64&version=ESV), [Daniel](https://www.biblegateway.com/passage/?search=Daniel+9%3A3-19&version=ESV), [Ezra & Nehemiah](https://www.biblegateway.com/passage/?search=nehemiah+8%3A6%2C8-9%3B+9%3A1-38&version=ESV), [Mary](https://www.biblegateway.com/passage/?search=luke+1%3A46-55&version=ESV).
### [Psalms.](https://www.biblegateway.com/reading-plans/bcp-daily-office/today?version=esv)

<hr>

# Scripture helps us hear from God.
We do not come *under* Scripture as a set of rules that must be obeyed [(Rom.7:4)](https://biblehub.com/romans/7-4.htm),  
but rather we come *into* it as a love story that now includes us [(Rom. 15:4)](https://biblehub.com/romans/15-4.htm).

Try spending some time daily this week letting God speak to you through this simple process of *Bible Discovery*:  
first look at the People, Places, Times, Actions and Repetitions (**Observation**);  
then ask and attempt to answer three specific questions about what the passage means (**Interpretation**);  
then still your thoughts and write down the one phrase that stood out to you personally (**Revelation**);  
then write down a specific action you will commit to do in response this week (**Application**);  
and finally write down the names of five (or more!) people you can share your revelation with (**Multiplication**).  

[<img style="max-width: 100%;" src=discovery.png alt="Observation, Interpretation, Revelation, Application, Multiplication">](https://mygospel.uk/discovery)
